## koa-log4js-base
### 安装
```javascript
npm install koa-log4js-base
```
### 使用
在启动文件 `bin/www` 或者 `app.js` 文件里面配置日志文件名称
```javascript
const LogAdapter = new (require('koa-log4js-base'))('test'); // 日志文件名称
```
然后在 `app.js` 服务配置的地方进行初始化
```javascript
app.use(LogAdapter.adapt());
```
